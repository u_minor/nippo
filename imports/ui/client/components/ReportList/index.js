import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data'

import ReportListItem from '../ReportListItem'

import { Reports } from '../../../../api/reports.js'

const ReportList = (props) => (
  <ul>
  {props.reports.map((report) => (
    <ReportListItem key={report._id} report={report} />
  ))}
  </ul>
)

export default withTracker(() => {
  Meteor.subscribe('allUserName')
  return {
    reports: Reports.find({}, { sort: { createdAt: -1 } }).fetch(),
  }
})(ReportList)
