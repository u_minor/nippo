import React, { Component } from 'react'

import AccountsUIWrapper from '../AccountsUIWrapper'

export default Header = (props) => (
  <header>
    Header
    <AccountsUIWrapper />
  </header>
)
