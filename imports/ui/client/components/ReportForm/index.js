import React, { Component } from 'react'

export default ReportForm = (props) => (
  <div>
    <form onSubmit={props.handleSubmit}>
      <textarea ref={props.inputRef}>
        {props.content}
      </textarea>
      <input
        type="submit"
        value={props.submitValue || '登録'}
      />
    </form>
  </div>
)
