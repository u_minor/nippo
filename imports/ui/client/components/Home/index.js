import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'

import ReportList from '../ReportList'
import ReportListItem from '../ReportListItem'

class Home extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userId: null,
      reportedAt: null,
    }
  }

  render() {
    return (
      <div>
        Home Page
        {this.props.currentUser &&
          <ul>
            <ReportListItem />
          </ul>
        }
        <ReportList />
      </div>
    )
  }
}

export default withTracker(() => {
  return {
    currentUser: Meteor.user(),
  }
})(Home)
