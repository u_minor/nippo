import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'

import ReportForm from '../ReportForm'

class ReportListItem extends Component {
  constructor(props) {
    super(props)

    this.state = {
      editMode: false
    }
  }

  handleInsert(e) {
    e.preventDefault()
    const content = this.inputContent.value.trim()
    Meteor.call('Reports.insert', content)
    this.inputContent.value = ''
  }

  handleUpdate(e) {
    e.preventDefault()
    const content = this.inputContent.value.trim()
    Meteor.call('Reports.update', this.props.report._id, content)
    this.inputContent.value = ''
    this.setState({ editMode: false })
  }

  handleDelete(e) {
    e.preventDefault()
    Meteor.call('Reports.delete', this.props.report._id)
  }

  handleToggleEditMode() {
    this.setState({ editMode: !this.state.editMode })
  }

  render() {
    return (
      <li>
        {this.props.report ? (
          <div>
            {!this.state.editMode ? (
              <div>
                <pre>{this.props.report.content}</pre>
                ({this.props.report.user && this.props.report.user.username})
              </div>
            ) : (
              <ReportForm
                handleSubmit={this.handleUpdate.bind(this)}
                inputRef={el => this.inputContent = el}
                content={this.props.report.content}
                submitValue="更新"
              />
            )}
            {this.props.currentUser && this.props.report.userId == this.props.currentUser._id &&
              <div>
                <input type="button" value="修正" onClick={this.handleToggleEditMode.bind(this)} />
                <input type="button" value="削除" onClick={this.handleDelete.bind(this)} />
              </div>
            }
          </div>
        ) : (
          <ReportForm
            handleSubmit={this.handleInsert.bind(this)}
            inputRef={el => this.inputContent = el}
          />
        )}
      </li>
    )
  }
}

export default withTracker(() => {
  return {
    currentUser: Meteor.user(),
  }
})(ReportListItem)
