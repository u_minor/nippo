import { Mongo } from 'meteor/mongo'
import { Meteor } from 'meteor/meteor'
import { check } from 'meteor/check'

export const Reports = new Mongo.Collection('Reports', { transform: (report) => {
  report.user = Meteor.users.findOne({ _id: report.userId })
  return report
}})

if (Meteor.isServer) {
  Meteor.publish('allUserName', () => (
    Meteor.users.find({}, { fields: { username: 1 } })
  ))
}

Meteor.methods({
  'Reports.insert'(content) {
    check(content, String)
    if (!this.userId) {
      throw new Meteor.Error('not-authorized')
    }
    Reports.insert({
      content,
      userId: this.userId,
      reportedAt: new Date(),
      createdAt: new Date(),
      updatedAt: new Date(),
    })
  },
  'Reports.update'(reportId, content) {
    check(reportId, String)
    check(content, String)
    const report = Reports.findOne(reportId)
    if (!report || report.userId !== this.userId) {
      throw new Meteor.Error('not-authorized')
    }
    Reports.update(reportId, { $set: {
      content,
      updatedAt: new Date(),
    }})
  },
  'Reports.delete'(reportId) {
    check(reportId, String)
    const report = Reports.findOne(reportId)
    if (!report || report.userId !== this.userId) {
      throw new Meteor.Error('not-authorized')
    }
    Reports.remove(reportId)
  },
})
