import React from 'react'
import { Router, Route } from 'react-router'
import createBrowserHistory from 'history/createBrowserHistory'

import Home from '../../ui/client/components/Home'
import Header from '../../ui/client/components/Header'
import Footer from '../../ui/client/components/Footer'

const browserHistory = createBrowserHistory()

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <div>
      <Header />
      <Route exact path="/" component={Home} />
      <Footer />
    </div>
  </Router>
)
