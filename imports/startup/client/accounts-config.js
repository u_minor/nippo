import { Accounts } from 'meteor/accounts-base'

//Accounts.config({
//  // disable create account
//  forbidClientAccountCreation: true,
//})
Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY',
})
